#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std; 

struct item
{
	string itemName;
	int gold;
};

// Ex 3-1
// Oh my god there has got to be a better way to do this
item* itemPull(vector<item*>& rucksack, bool& out)
{
	int itemRoll = rand() % 5;

	if (itemRoll == 0)
	{
		item* MythrilOre = new item;

		MythrilOre->itemName = "Mythril Ore";
		MythrilOre->gold = 100;

		cout << "You got: " << MythrilOre->itemName << "!" << endl;

		rucksack.push_back(MythrilOre);
	}

	if (itemRoll == 1)
	{
		item* SharpTalon = new item;

		SharpTalon->itemName = "Sharp Talon";
		SharpTalon->gold = 50;

		cout << "You got: " << SharpTalon->itemName << "!" << endl;

		rucksack.push_back(SharpTalon);
	}

	if (itemRoll == 2)
	{
		item* ThickLeather = new item;

		ThickLeather->itemName = "Thick Leather";
		ThickLeather->gold = 5;

		cout << "You got: " << ThickLeather->itemName << "!" << endl;

		rucksack.push_back(ThickLeather);
	}

	if (itemRoll == 3)
	{
		item* Jellopy = new item;

		Jellopy->itemName = "Jellopy";
		Jellopy->gold = 5;

		cout << "You got: " << Jellopy->itemName << "!" << endl;

		rucksack.push_back(Jellopy);
	}

	if (itemRoll == 4)
	{
		item* CursedStone = new item;

		CursedStone->itemName = "Cursed Stone";
		CursedStone->gold = 0;

		cout << "You got: " << CursedStone->itemName << "!" << endl;

		out = false; 
	}

	return 0; 
}

void welcomePlayer(int& playerGold)
{
	char playerResponse;

	cout << "Welcome to the Cave of Secrets adventurer!" << endl;
	cout << "That will be 25 Gold for the Entrance Fee" << endl;
	cout << "I'm in! [Y]  ||  No thanks [N] ";
	cin >> playerResponse;

		switch (playerResponse)
		{
		default: cout << "Go in you say? Okie dokies!" << endl;

		case 'y': cout << "Happy hunting! And BEWARE the Cursed Stone." << endl; 
					playerGold -= 25;
					break; 

		case 'n': cout << "No turning back now!" << endl;
					playerGold -= 25;
					cout << "25 pieces of gold was taken from you." << endl;
					cout << "You now have " << playerGold << " pieces of Gold left." << endl;
					cout << "Happy hunting! And BEWARE the Cursed Stone." << endl;
		}
}

// Ex 3-2
void enterDungeon(int& playerGold, bool& yesOrNo, vector <item*>& rucksack, int bonus)
{
	char playerResponse; 

	cout << "Do you wish to delve deeper?" << endl;
	cout << "Yes [Y]  ||  No [N] ";
	cin >> playerResponse; 
	cout << endl;

	switch (playerResponse)
	{
		default: cout << "What was that? I didn't quite hear you. Speak up, son!" << endl;

		case 'y': 
				itemPull(rucksack, yesOrNo);
				bonus++;
				cout << endl;

				system("pause");
				system("cls"); 
				break;

		case 'n': 
			cout << "You got the following items: " << endl;
			cout << endl;

			int i = 0;
			int multiplyer = 1;
			for (int x = 0; x < rucksack.size(); x++)
			{
				item* currentItem = rucksack[i];

				cout << "Current Gold: " << playerGold << endl;
				cout << currentItem->itemName << " | " << currentItem->gold << " gold" << " x" << multiplyer << " Bonus multiplyer" << endl;

				playerGold += currentItem->gold * multiplyer;
				cout << "Current Gold: " << playerGold << endl;
				cout << endl;

				multiplyer++;
				i++;
			}

			cout << "Amount of Gold earned: " << playerGold << endl; 
			cout << endl;

			system("pause");
			system("cls");

			yesOrNo = false; 
	}

}

// Ex 3-3
void endGame(int& gold)
{
	if (gold >= 500)
	{
		cout << "With your money full of pockets, you gallop away on your noble steed into the sunset." << endl;
		cout << "=== GOOD END ===" << endl; 
	}
	else
	{
		cout << "You leave the Cave of Secrets, injured and without doubling your Gold." << endl; 
		cout << "Damn. You don't even have insurance." << endl; 
		cout << "Maybe you should try getting a stable job instead." << endl;
		cout << "=== SAD END ===" << endl;
	}
}

int main()
{
	srand(time(0)); 

	vector <item*> rucksack;
	int playerGold = 50; 
	bool inDungeon = true;
	int bonus = 0; 
 
	welcomePlayer(playerGold);

	cout << endl;
	cout << "=== GAME START ===" << endl; 
	cout << endl;

	while (inDungeon == true)
	{
		enterDungeon(playerGold, inDungeon, rucksack, bonus);
		bonus++;
	}

	endGame(playerGold);
}