#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std; 

// Ex 2 - 1 
// Different versions - using populateVector3 for main 
// populateVector1 and 2 only puts 4 items in the inventory 
void populateVector1(vector <string>& inventory)
{
	inventory.push_back("RedPotion");
	inventory.push_back("Elixir");
	inventory.push_back("EmptyBottle");
	inventory.push_back("BluePotion");
}

void populateVector2(vector <string>& inventory)
{
	int x = 0;

	string array[] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };

	for (int populate = 0; populate < 4; populate++)
	{
		inventory.push_back(array[x]);
		x++;
	}
}


void populateVector3(vector <string>& inventory)
{
	string array[] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };

	for (int populate = 0; populate < 10; populate++)
	{
		inventory.push_back(array[rand() % 4]);
	}
}

// Ex 2 - 2 
// Two versions: one for populateVector3 and one for populateVector1 and 2
void printVector1and2(const vector<string>& inventory)
{
	int i = 0;
	for (int print = 0; print < 10; print++)
	{
		cout << inventory[rand() % 4] << endl;
	}

}

void printVector3(const vector<string>& inventory)
{
	int i = 0;
	for (int print = 0; print < inventory.size(); print++)
	{
		cout << inventory[i] << endl;
		i++;
	}
}

// Ex 2 - 3 
// Use either one: One uses count, one uses a counter
void searchVectorCount(const vector<string>& inventory)
{
	string keyword;

	cout << "Type in the item you want to count: ";
	cin >> keyword;

	cout << "Number of " << keyword << ": " << count(inventory.begin(), inventory.end(), keyword) << endl;
}

void searchVector(const vector<string>& inventory)
{
	string keyword;
	int counter = 0;

		cout << "Type in the item you want to count: ";
		cin >> keyword;

	for (int x = 0; x < 10; x++)
	{
		if (keyword == inventory[x])
		{
			counter++;
		}
	}

	cout << counter;
	cout << endl;
}

// Ex 2 - 4 
// Two versions: one to remove the first instance, one to remove all instances
// Using removeVectorAll
void removeVectorSingle(vector <string>& inventory)
{
	string keyword; 

	cout << "Type in an item you want to remove: ";
	cin >> keyword;
	
	for (int x = 0; x < inventory.size(); x++)
	{
		if (keyword == inventory[x])
		{
			inventory.erase(inventory.begin() + x);
			break;
		}
	}
}

void removeVectorAll(vector <string>& inventory)
{
	string keyword;

	cout << "Type in an item you want to remove: ";
	cin >> keyword;

	for (int x = 0; x < 10; x++)
	{
		for (int x = 0; x < inventory.size(); x++)
		{
			if (keyword == inventory[x])
			{
				inventory.erase(inventory.begin() + x);
			}
		}
	}
}

int main()
{
	srand(time(0));
	vector <string> inventory;

	populateVector3(inventory);
	printVector3(inventory); 
	cout << endl;

	searchVector(inventory); 
	cout << endl;

	removeVectorAll(inventory); 
	cout << endl;

	printVector3(inventory);

}
