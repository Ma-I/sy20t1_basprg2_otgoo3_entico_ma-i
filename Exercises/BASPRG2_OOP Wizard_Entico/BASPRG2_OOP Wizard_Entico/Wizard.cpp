#include "Wizard.h"

void Wizard::attack(Wizard* attacker, Wizard* target)
{
	int attack = (rand() % (mMaxDam - mMinDam + 1)) + mMinDam;
	int addMP = (rand() % (mGetMaxMP - mGetMinMP + 1)) + mGetMinMP;

	cout << attacker->mName << " deals " << attack << " to "<< target->mName << " with his staff" << endl;
	target->mHP -= attack; 
	attacker->mMP += addMP;
}
