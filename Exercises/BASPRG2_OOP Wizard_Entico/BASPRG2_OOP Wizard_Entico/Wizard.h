#pragma once
#include <iostream>
#include <string>

using namespace std; 

class Wizard
{
public: 

	string mName; 
	int mHP; 
	int mMP;

	int mMinDam = 10;
	int mMaxDam = 15;

	int mGetMinMP = 10; 
	int mGetMaxMP = 20; 

	void attack(Wizard* attacker, Wizard* target);
};

