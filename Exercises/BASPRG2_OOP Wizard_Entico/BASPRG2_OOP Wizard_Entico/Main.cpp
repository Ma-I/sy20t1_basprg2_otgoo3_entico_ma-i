#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include "WizardSpell.h"

using namespace std; 

int main()
{
	// Attacking generates 10~20 MP(random).

	//  Wizard 1
	Wizard* FireWizard = new Wizard();

	FireWizard->mName = "Vulkan";
	FireWizard->mHP = 250;
	FireWizard->mMP = 0; 

	WizardSpell* FireSpell = new WizardSpell(); 
	FireSpell->mSpellName = "Wrath of our EMPRAH!"; 
	
	// Wizard 2 
	Wizard* DarkWizard = new Wizard();
	
	DarkWizard->mName = "Konrad";
	DarkWizard->mHP = 250;
	DarkWizard->mMP = 0;

	WizardSpell* DarkSpell = new WizardSpell();
	DarkSpell->mSpellName = "~ Chaos Undivided ~";

	while (FireWizard->mHP > 0 && DarkWizard->mHP > 0)
	{
		// UI
		cout << FireWizard->mName << endl;
		cout << FireWizard->mHP << " HP || " << FireWizard->mMP << " MP" << endl;

		cout << DarkWizard->mName << endl;
		cout << DarkWizard->mHP << " HP || " << DarkWizard->mMP << " MP" << endl;
		cout << "========================" << endl; 
		cout << endl;

		// play round :: attack
		FireWizard->attack(FireWizard, DarkWizard);
		cout << endl;
		DarkWizard->attack(DarkWizard, FireWizard);
		cout << endl;

		system("pause");
		system("cls");

		cout << FireWizard->mName << endl;
		cout << FireWizard->mHP << " HP || " << FireWizard->mMP << " MP" << endl;

		cout << DarkWizard->mName << endl;
		cout << DarkWizard->mHP << " HP || " << DarkWizard->mMP << " MP" << endl;
		cout << "========================" << endl;
		cout << endl;

		// play round :: cast 
		if (FireWizard->mMP >= 50)
		{
			FireSpell->castSpell(FireWizard, DarkWizard);
			cout << endl;
		}
		if (DarkWizard->mMP >= 50)
		{
			DarkSpell->castSpell(DarkWizard, FireWizard);
			cout << endl;
		}

		system("pause");
		system("cls");
	}

	if (FireWizard->mHP > DarkWizard->mHP)
	{
		cout << FireWizard->mName << " WINS!" << endl; 
	}
	if (DarkWizard->mHP > FireWizard->mHP)
	{
		cout << DarkWizard->mName << " WINS!" << endl;
	}

	delete FireWizard; 
	delete DarkWizard;
	delete FireSpell;
	delete DarkSpell;

}
