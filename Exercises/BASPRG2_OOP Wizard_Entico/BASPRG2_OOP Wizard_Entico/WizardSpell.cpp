#include "WizardSpell.h"
#include "Wizard.h"

void WizardSpell::castSpell(Wizard* caster, Wizard* target)
{
	int spellDamage = (rand() % (mMaxDam - mMinDam + 1)) + mMinDam;

	cout << caster->mName << " casts " << this->mSpellName << " on " << target->mName << endl; 
	cout << this->mSpellName << " deals " << spellDamage << " damage to " << target->mName << endl; 
	target->mHP -= spellDamage; 
	caster->mMP -= mCostMP;
}
