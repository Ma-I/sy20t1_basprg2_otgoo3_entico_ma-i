#pragma once
#include <iostream>
#include <string>

using namespace std; 

class Wizard;

class WizardSpell
{
public: 

	string mSpellName; 
	int mCostMP = 50;

	int mMinDam = 40;
	int mMaxDam = 60;

	void castSpell(Wizard* caster, Wizard* target);
};

