#include <iostream>
#include <time.h>
#include <string.h>

using namespace std;

// player starts with 1000 gold 
// player bets gold that is not less than or equal to 0; or greater than player gold 
// Ai rolls 2 dice sum of dice is value of bet
// player rolls 2 dice too 
// if value is higher than AI, player wins 
// if player rolls snake eyes (1-1) 
// if AI and plater roll the same value, it is a draw and no one wins
// applicable with snake eyes 
// will end when player gets 0 

// Ex 1-1 ver 1
// using this one
void bet1(int& gold, int& bet1)
{
	cout << "YOUR GOLD: " << gold << endl; 
	cout << "PLACE YOUR BET: "; 
	cin >> bet1; 

	if (bet1 < 0 || bet1 > gold)
	{
		do
		{
			cout << "PLEASE PLACE VALID BET: "; 
			cin >> bet1; 
		} while (bet1 < 0 || bet1 > gold); 
	}

	cout << endl;

}

// Ex 1-1 ver 2 
// hasn't been modified since last two edits
int bet2(int gold)
{
	int bet2; 
	cout << "YOUR GOLD: " << gold << endl;
	cout << "PLACE YOUR BET: ";
	cin >> bet2;

	if (bet2 < 0 || bet2 > gold)
	{
		do
		{
			cout << "PLEASE PLACE VALID BET: ";
			cin >> bet2;
		} while (bet2 < 0 || bet2 > gold);
	}

	else
	{
		int placedBet = gold - bet2;
		cout << "BET PLACED... " << endl;
		cout << "GOLD LEFT: " << placedBet;

	}

	return bet2; 
}

// Ex 1-2
void RollAndSum(int& dice1, int& dice2, int& sum)
{
	dice1 = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
	sum = dice1 + dice2;

	cout << endl;
	cout << "ROLLS: " << dice1 << ", " << dice2 << endl; 
	cout << "TOTAL: " << sum; 
	cout << endl; 
}

// Ex 1-3
int payOut(int& gold, int& bet, int aiSum, int playerSum)
{
	if (playerSum == 2)
	{
		cout << "SNAKE EYES" << endl;
		cout << endl;

		gold *= 3;
		return gold; // so that this thing stops checking
	}
	if (aiSum > playerSum)
	{
		cout << "LOSE" << endl; 
		cout << endl;

		gold -= bet;
	}
	if (aiSum < playerSum)
	{
		cout << "WIN" << endl;
		cout << endl;

		gold += bet; 
	}
	if (aiSum == playerSum)
	{
		cout << "DRAW" << endl;
		cout << endl;
	}

	return gold; 
}

int main()
{
	srand(time(0));
	int playerGold = 1000;
	int playerBet;

	int aiDice1;
	int aiDice2;
	int aiSum;

	int playerDice1;
	int playerDice2;
	int playerSum;

while (playerGold > 0)
{
	bet1(playerGold, playerBet);

	cout << "AI's TURN... ";
	RollAndSum(aiDice1, aiDice2, aiSum);
	cout << endl;
	cout << "YOUR TURN... ";
	RollAndSum(playerDice1, playerDice2, playerSum);
	cout << endl;

	payOut(playerGold, playerBet, aiSum, playerSum);
}

}