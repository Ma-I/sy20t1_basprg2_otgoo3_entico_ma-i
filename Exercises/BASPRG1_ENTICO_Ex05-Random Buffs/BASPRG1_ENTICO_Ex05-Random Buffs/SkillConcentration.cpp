#include "SkillConcentration.h"

SkillConcentration::SkillConcentration()
	: SkillBase("Concentrate", 2)
{
}

void SkillConcentration::useConcentration(Unit* player)
{
	cout << "You used CONCENTRATE" << endl;
	player->setDEX(player->getDEX() + this->getBuffStat());
	cout << this->getBuffStat() << " was added to your current DEX..." << endl;
}
