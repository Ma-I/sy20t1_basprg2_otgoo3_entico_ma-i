#pragma once
#include <iostream>
#include <string>
#include "SkillBase.h"
#include "Unit.h"

using namespace std;

class SkillConcentration : public SkillBase
{
public:

	SkillConcentration();

	void useConcentration(Unit* player);

};