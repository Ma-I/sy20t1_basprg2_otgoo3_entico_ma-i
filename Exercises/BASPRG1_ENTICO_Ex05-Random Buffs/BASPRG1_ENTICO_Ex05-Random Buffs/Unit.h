#pragma once
#include <string>

using namespace std;

class Unit
{
public: 

	Unit(string name, int hp, int pow, int vit, int agi, int dex);

	// Getters
	string getName();
	int getHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();

	// Setters
	void setName(string name);
	void setHP(int hp);
	void setPOW(int pow);
	void setVIT(int vit);
	void setAGI(int agi);
	void setDEX(int dex);

private: 

	string mName; 
	int mHP;
	int mPOW; 
	int mVIT; 
	int mDEX; 
	int mAGI; 
};

