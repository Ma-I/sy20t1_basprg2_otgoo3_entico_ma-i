#include "SkillHaste.h"

SkillHaste::SkillHaste()
	: SkillBase("Haste", 2)
{
}

void SkillHaste::useHaste(Unit* player)
{
	cout << "You used HASTE" << endl;
	player->setAGI(player->getAGI() + this->getBuffStat());
	cout << this->getBuffStat() << " was added to your current AGI..." << endl;
}
