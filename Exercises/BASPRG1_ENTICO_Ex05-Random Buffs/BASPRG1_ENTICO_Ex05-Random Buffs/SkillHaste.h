#pragma once
#include <iostream>
#include <string>
#include "SkillBase.h"
#include "Unit.h"

using namespace std;

class SkillHaste : public SkillBase
{
public:

	SkillHaste();

	// won't "heal" per se. Will just add to the current max HP
	void useHaste(Unit* player);

};
