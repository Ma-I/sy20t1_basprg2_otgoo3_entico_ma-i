#include "SkillBase.h"

SkillBase::SkillBase(string name, int buffStat)
{
	mSkillName = name;
	mBuffStat = buffStat;
}

string SkillBase::getSkillName()
{
	return mSkillName;
}

int SkillBase::getBuffStat()
{
	return mBuffStat;
}

void SkillBase::setSkillName(string name)
{
	mSkillName = name;
}

void SkillBase::setBuffStat(int buff)
{
	mBuffStat = buff;
}
