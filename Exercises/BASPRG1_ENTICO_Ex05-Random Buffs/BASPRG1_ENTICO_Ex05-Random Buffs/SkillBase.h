#pragma once
#include <string>

using namespace std; 

class SkillBase
{
public: 

	SkillBase(string name, int buffStat); 

	string getSkillName(); 
	int getBuffStat(); 

	void setSkillName(string name);
	void setBuffStat(int buff); 

private: 

	string mSkillName; 
	int mBuffStat; 
};

