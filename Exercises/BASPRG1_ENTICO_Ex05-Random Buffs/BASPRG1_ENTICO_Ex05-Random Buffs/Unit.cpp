#include "Unit.h"

Unit::Unit(string name, int hp, int pow, int vit, int agi, int dex)
{
    mName = name; 
    mHP = hp; 
    mPOW = pow; 
    mVIT = vit; 
    mAGI = agi; 
    mDEX = dex; 
}

string Unit::getName()
{
    return mName;
}

int Unit::getHP()
{
    return mHP;
}

int Unit::getPOW()
{
    return mPOW;
}

int Unit::getVIT()
{
    return mVIT;
}

int Unit::getAGI()
{
    return mAGI;
}

int Unit::getDEX()
{
    return mDEX;
}

void Unit::setName(string name)
{
    mName = name; 
}

void Unit::setHP(int hp)
{
    mHP = hp;
}

void Unit::setPOW(int pow)
{
    mPOW = pow; 
}

void Unit::setVIT(int vit)
{
    mVIT = vit;
}

void Unit::setAGI(int agi)
{
    mAGI = agi; 
}

void Unit::setDEX(int dex)
{
    mDEX = dex;
}
