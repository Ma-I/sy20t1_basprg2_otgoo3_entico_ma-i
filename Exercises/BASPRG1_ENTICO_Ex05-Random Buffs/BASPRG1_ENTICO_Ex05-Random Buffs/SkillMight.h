#pragma once
#include <iostream>
#include <string>
#include "SkillBase.h"
#include "Unit.h"

using namespace std; 

class SkillMight : public SkillBase
{
public: 

	SkillMight();

	void useMight(Unit* player);

};