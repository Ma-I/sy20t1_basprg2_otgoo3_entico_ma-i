#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
#include "SkillBase.h"
#include "SkillConcentration.h"
#include "SkillHaste.h"
#include "SkillHeal.h"
#include "SkillIronSkin.h"
#include "SkillMight.h"

using namespace std; 

void gameStart(Unit* player)
{
	// cin values
	string name; 
	int hp; 
	int pow; 
	int vit; 
	int agi; 
	int dex; 

	cout << "Welcome to the testing grounds" << endl;
	cout << "What is your name?: "; 
	cin >> name; 

	player->setName(name); 

	cout << "Please fill out your stats on this form before proceeding..." << endl; 
	cout << "* the man hands you a form *" << endl; 
	cout << "* you start filling it out *" << endl; 
	cout << "---------" << endl; 

	system("pause"); 
	system("cls");

	cout << "What is your HP?: "; 
	cin >> hp; 
	player->setHP(hp);

	//while (cin.fail())
	//{
	//	cin.clear();
	//	cin.ignore(INT_MAX); 
	//	cout << "You can only enter numbers" << endl; 
	//	cout << "Enter a number" << endl;
	//	cin >> hp;
	//} << won't add bc I haven't found a way to NOT copy paste it after each cin rip but it works 

	cout << "What is your POW?: "; 
	cin >> pow; 
	player->setPOW(pow);

	cout << "What is your VIT?: ";
	cin >> vit; 
	player->setVIT(vit);

	cout << "What is your AGI?: "; 
	cin >> agi; 
	player->setAGI(agi);

	cout << "What is your DEX?: "; 
	cin >> dex; 
	player->setDEX(dex);

	system("pause");
	system("cls");
}

void overheadUI(Unit* player)
{
	cout << "PLAYER: " << player->getName() << endl;
	cout << "HP: " << player->getHP() << endl;
	cout << "POW: " << player->getPOW() << endl;
	cout << "DEX: " << player->getDEX() << endl;
	cout << "AGI: " << player->getAGI() << endl;
	cout << "VIT: " << player->getVIT() << endl;
}

void useRandomSkill(Unit* player, SkillConcentration* concentrate, SkillHaste* haste, SkillHeal* heal, SkillIronSkin* ironSkin, SkillMight* might)
{
	int dice = rand() % 5 + 1;

	if (dice == 1)
	{
		concentrate->useConcentration(player);
	}
	if (dice == 2)
	{
		haste->useHaste(player);
	}
	if (dice == 3)
	{
		heal->useHeal(player);
	}
	if (dice == 4)
	{
		ironSkin->useIronSkin(player);
	}
	if (dice == 5)
	{
		might->useMight(player);
	}
}

int main()
{
	srand(time(NULL));
	char loop = 'i';

	// default placeholders
	// hp, pow, vit, agi, dex
	Unit* player = new Unit("default", 1, 1, 1, 1, 1);

	SkillConcentration* concentrate = new SkillConcentration; 
	SkillHaste* haste = new SkillHaste; 
	SkillHeal* heal = new SkillHeal; 
	SkillIronSkin* ironSkin = new SkillIronSkin; 
	SkillMight* might = new SkillMight;

	gameStart(player); 
	
	cout << endl;
	cout << "The door to the training room opens and you commence training..." << endl;

	while (loop != 'x')
	{
		useRandomSkill(player, concentrate, haste, heal, ironSkin, might);

		system("pause");
		system("cls");

		overheadUI(player);

		cout << endl;
		cout << "Input 'x' to EXIT and any other character to continue..." << endl;
		cout << "Exit?: ";
		cin >> loop; 
		cout << endl;

		system("pause");
		system("cls");
	}
}
