#include "SkillIronSkin.h"

SkillIronSkin::SkillIronSkin()
	:SkillBase("Iron Skin", 2)
{
}

void SkillIronSkin::useIronSkin(Unit* player)
{
	cout << "You used IRON SKIN" << endl;
	player->setVIT(player->getVIT() + this->getBuffStat());
	cout << this->getBuffStat() << " was added to your current VIT..." << endl;
}
