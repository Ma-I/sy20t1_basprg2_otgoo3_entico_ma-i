#pragma once
#include <iostream>
#include <string>
#include "SkillBase.h"
#include "Unit.h"

using namespace std; 

class SkillHeal : public SkillBase
{
public: 

	SkillHeal(); 

	// won't "heal" per se. Will just add to the current max HP
	void useHeal(Unit* player); 

};
