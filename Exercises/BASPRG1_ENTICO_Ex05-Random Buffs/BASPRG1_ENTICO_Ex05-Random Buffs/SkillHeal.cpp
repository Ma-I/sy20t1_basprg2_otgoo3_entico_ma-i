#include "SkillHeal.h"

SkillHeal::SkillHeal()
	: SkillBase("Heal", 10)
{

}

void SkillHeal::useHeal(Unit* player)
{
	cout << "You used HEAL" << endl; 
	player->setHP(player->getHP() + this->getBuffStat());
	cout << this->getBuffStat() << " was added to your current HP..." << endl; 
}
