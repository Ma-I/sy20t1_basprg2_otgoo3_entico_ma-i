#include "SkillMight.h"

SkillMight::SkillMight()
	:SkillBase("Might", 2)
{
}

void SkillMight::useMight(Unit* player)
{
	cout << "You used MIGHT" << endl;
	player->setPOW(player->getPOW() + this->getBuffStat());
	cout << this->getBuffStat() << " was added to your current POW..." << endl;
}
