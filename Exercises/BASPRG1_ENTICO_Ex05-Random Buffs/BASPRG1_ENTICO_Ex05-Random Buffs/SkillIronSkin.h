#pragma once
#include <iostream>
#include <string>
#include "SkillBase.h"
#include "Unit.h"

using namespace std;

class SkillIronSkin : public SkillBase
{
public:

	SkillIronSkin();

	void useIronSkin(Unit* player);
};
