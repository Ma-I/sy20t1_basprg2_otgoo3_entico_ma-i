#ifndef NODE_H

#define NODE_H

#include <iostream>
#include <time.h>
#include <string>

using namespace std;

struct Node
{
    string name;
    Node* next = NULL;
};

#endif// NODE_H

void activeMembers(Node* membersList, int& members)
{
    cout << "Remaining soldiers: " << endl;

    for (int start = 0; start < members; start++)
    {
        cout << membersList->name << endl;
        membersList = membersList->next;
    }

    cout << "============" << endl;
}

Node* listName(int& members)
{
    int randomize = rand() % 5 + 1; 

    Node* soldierOne = new Node;
    soldierOne->name = "Snow";

    Node* soldierTwo = new Node;
    soldierTwo->name = "Alliser";

    Node* soldierThree = new Node;
    soldierThree->name = "Othell";

    Node* soldierFour = new Node;
    soldierFour->name = "Sam";

    Node* soldierFive = new Node;
    soldierFive->name = "Janos";

    soldierOne->next = soldierTwo;
    soldierTwo->next = soldierThree;
    soldierThree->next = soldierFour;
    soldierFour->next = soldierFive;
    soldierFive->next = soldierOne;

    // randomization - start
    Node* current = soldierOne;

    cout << "The King is choosing who to give the cape first to..." << endl; 

    for (int player = 1; player <= randomize; player++)
    {
        current = current->next;
    }

    cout << current->name << " was chosen to move first" << endl;
    cout << "=============" << endl;

    system("pause");
    system("cls"); 

    // iteration and deletion

    int membersLeft = 4;

    do {
        activeMembers(current, members);
        Node* deleteSoldier = current;
        Node* holder = new Node;

        int roulette = rand() % members + 1;

        cout << current->name << " rolls " << roulette << endl;

        for (int player = 1; player <= roulette; player++)
        {
            current = current->next;
            holder = deleteSoldier;

            deleteSoldier = deleteSoldier->next;
        }
        cout << current->name << " was eliminated" << endl;
        cout << "=============" << endl;

        holder->next = deleteSoldier->next;

        delete deleteSoldier;
        deleteSoldier = nullptr;

        current = holder;
        current = current->next;
        cout << "The cape was passed to " << current->name << endl;

        membersLeft--;
        members--; 

        system("pause");
        system("cls"); 

    } while (membersLeft != 0);

    cout << current->name << " was chosen to seek out reinforcements..." << endl; 


    return 0;
}

int main()
{
    srand(time(NULL));
    int members = 5; 

    listName(members);

}
