#include <iostream>
#include <time.h>
#include<string>
#include <vector>

using namespace std;

// using FOUR inventories bc of the .erase command
// not using for loop bc of that one different card

void cardsPlayerSlave(vector <string>& slaveInventory)
{
	slaveInventory.push_back("Slave");
	slaveInventory.push_back("Citizen");
	slaveInventory.push_back("Citizen");
	slaveInventory.push_back("Citizen");
	slaveInventory.push_back("Citizen");
}

void cardsPlayerEmperor(vector <string>& emperorInventory)
{
	emperorInventory.push_back("Emperor");
	emperorInventory.push_back("Citizen");
	emperorInventory.push_back("Citizen");
	emperorInventory.push_back("Citizen");
	emperorInventory.push_back("Citizen");
}

void cardsAiSlave(vector <string>& slaveInventory)
{
	slaveInventory.push_back("Slave");
	slaveInventory.push_back("Citizen");
	slaveInventory.push_back("Citizen");
	slaveInventory.push_back("Citizen");
	slaveInventory.push_back("Citizen");
}

void cardsAiEmperor(vector <string>& emperorInventory)
{
	emperorInventory.push_back("Emperor");
	emperorInventory.push_back("Citizen");
	emperorInventory.push_back("Citizen");
	emperorInventory.push_back("Citizen");
	emperorInventory.push_back("Citizen");
}

// Bet function
void bet(int& bet, int& mmLeft)
{
	cout << "Alright Kaiji, what will your bet be? : ";
	cin >> bet;
	cout << endl;

	if (0 >= bet || bet > mmLeft)
	{
		while (bet <= 0 || bet > mmLeft)
		{
			cout << "PLEASE ENTER A VALID BET... ";
			cin >> bet;
			cout << endl;
		}
	}
}

// Introduction text 
void begin()
{
	cout << "As Tonegawa's assistants attach the drill to you ear, you can see a smile spreading across his face." << endl;
	system("pause");
	system("cls");
	cout << "The chandeliers above you seem to glow too brightly today." << endl;
	cout << "Afterall, this may be your last game." << endl;
	system("pause");
	system("cls");
	cout << "Well, that is, if you fail to win." << endl;
	system("pause");
	system("cls");
	cout << "Your cards are handed to you, and you and Tonegawa share a moment of silence." << endl;
	cout << "He smirks and says: Your move Kaiji." << endl;
	system("pause");
	system("cls");
	cout << "You have exactly 30mm you can use to bet." << endl;
	cout << "You are nervous" << endl;
	cout << "It seems like you'll lose your hearing before you can win that 20 million." << endl;
	system("pause");
	system("cls");
	cout << "No, snap out of it! - you tell yourself" << endl;
	cout << "There is still a possibility to win this." << endl;
	cout << "And there is only way to find out if you'll pull through." << endl;
	system("pause");
	system("cls");
	cout << "It's game time." << endl;
	system("pause");
	system("cls");
}

// Probably too many functions to keep track of once we separate this into two - one for the cout, and one for the conditionals (and maybe another for the bet)
// There must be a better way to go about this. The conditionals are making me cross-eyed
// vector subscript out of range error happens RANDOMLY
// Either mmLeft or moneyEarned (depending on which one goes first) shows reference index instead of actual value (mmLeft), or minuses 100k and adds 20k for the first round (moneyEarned)

// Maybe I should have made a function for the VS bit to save up on the lines

void emperorPlay(vector <string>& playerEmperor, vector <string>& AIslave, int& mmLeft, int& mmBet, int& moneyEarned)
{
	int chooseCard;
	int pickAI = rand() % AIslave.size();
	int number = 0;

	cout << "Now which card will it be? : ";
	cout << endl;

	for (int index = 0; index < playerEmperor.size(); index++)
	{
		cout << "[" << number << "]" << playerEmperor[index];
		cout << endl;
		number++;
	}

		do {
			cin >> chooseCard;
			cout << endl;

			if (0 > chooseCard || chooseCard > playerEmperor.size())
			{
				cout << "Wait... That card isn't in this deck... Let's try again... : ";
			}
		} while (0 > chooseCard || chooseCard > playerEmperor.size());

		system("cls");

			// conditionals

			// draw
			// remove citizen
			if (playerEmperor[chooseCard] == "Citizen" && AIslave[pickAI] == "Citizen")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerEmperor[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIslave[pickAI] << " ]" << endl;


				AIslave.erase(AIslave.begin() + pickAI);
				playerEmperor.erase(playerEmperor.begin() + chooseCard);

				cout << endl;
				cout << "DRAW" << endl;

				system("pause");
				system("cls");
			}

			// player lose
			// minus mm
			if (playerEmperor[chooseCard] == "Emperor" && AIslave[pickAI] == "Slave")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerEmperor[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIslave[pickAI] << " ]" << endl;

				cout << endl;
				cout << "YOU LOSE" << endl;
				cout << "BBRRRRRRRR! The drill inches closer to your eardrum..." << endl;

				system("pause");
				system("cls");

				mmLeft -= mmBet;

				if (mmLeft > 0)
				{
					cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
					bet(mmBet, mmLeft);
				}

				system("pause");
				system("cls");
			}

			// win
			// more money 
			if (playerEmperor[chooseCard] == "Citizen" && AIslave[pickAI] == "Slave")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerEmperor[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIslave[pickAI] << " ]" << endl;

				cout << endl;
				cout << "YOU WIN" << endl;
				cout << "As the assisants give you your money, you can see Tonegawa starting to sweat a bit..." << endl;

				system("pause");
				system("cls");

				moneyEarned += mmBet * 100000;

				if (mmLeft > 0)
				{
					cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
					bet(mmBet, mmLeft);
				}

				system("pause");
				system("cls");
			}

			// win 
			// more money
			if (playerEmperor[chooseCard] == "Emperor" && AIslave[pickAI] == "Citizen")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerEmperor[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIslave[pickAI] << " ]" << endl;

				AIslave.erase(AIslave.begin() + pickAI);

				cout << endl;
				cout << "YOU WIN" << endl;
				cout << "As the assisants give you your money, you can see Tonegawa starting to sweat a bit..." << endl;

				system("pause");
				system("cls");

				moneyEarned += mmBet * 100000;

				if (mmLeft > 0)
				{
					cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
					bet(mmBet, mmLeft);
				}

				system("pause");
				system("cls");
			}
}

// Slave round 
void slavePlay(vector <string>& playerSlave, vector <string>& AIEmperor, int& mmLeft, int& mmBet, int& moneyEarned)
{
	int chooseCard;
	int pickAI = rand() % AIEmperor.size();
	int number = 0;

	cout << "Now which card will it be? : ";
	cout << endl;

	for (int index = 0; index < playerSlave.size(); index++)
	{
		cout << "[" << number << "]" << playerSlave[index];
		cout << endl;
		number++;
	}

		do {
			cin >> chooseCard;
			cout << endl;

			if (0 > chooseCard || chooseCard > playerSlave.size())
			{
				cout << "Wait... That card isn't in this deck... Let's try again... : ";
			}
		} while (0 > chooseCard || chooseCard > playerSlave.size());

		system("cls");

			// draw
			// remove citizen
			if (playerSlave[chooseCard] == "Citizen" && AIEmperor[pickAI] == "Citizen")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerSlave[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIEmperor[pickAI] << " ]" << endl;

				AIEmperor.erase(AIEmperor.begin() + pickAI);
				playerSlave.erase(playerSlave.begin() + chooseCard);

				cout << "DRAW" << endl;

				system("pause");
				system("cls");
			}

			// win
			// moooore money
			if (playerSlave[chooseCard] == "Slave" && playerSlave[pickAI] == "Emperor")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerSlave[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIEmperor[pickAI] << " ]" << endl;

				moneyEarned += mmBet * 500000;
				playerSlave.erase(playerSlave.begin() + 1);

				cout << "YOU WIN" << endl;

				if (mmLeft > 0)
				{
					cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
					bet(mmBet, mmLeft);
				}

				system("pause");
				system("cls");
			}

			// lose
			// lose mm
			if (playerSlave[chooseCard] == "Slave" && playerSlave[pickAI] == "Citizen")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerSlave[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIEmperor[pickAI] << " ]" << endl;

				cout << endl;
				cout << "YOU LOSE" << endl;
				cout << "BBRRRRRRRR! The drill inches closer to your eardrum..." << endl;

				system("pause");
				system("cls");

				mmLeft -= mmBet;

				if (mmLeft > 0)
				{
					cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
					bet(mmBet, mmLeft);
				}

				system("pause");
				system("cls");
			}

			// lose
			// lose mm 
			if (playerSlave[chooseCard] == "Citizen" && playerSlave[pickAI] == "Emperor")
			{
				cout << "You and Tonegawa both flip your cards open..." << endl;
				cout << endl;
				cout << "You" << "[ " << playerSlave[chooseCard] << " ]";
				cout << "     " << " VS. " << "     ";
				cout << "Tonegawa" << "[ " << AIEmperor[pickAI] << " ]" << endl;

				cout << endl;
				cout << "YOU LOSE" << endl;
				cout << "BBRRRRRRRR! The drill inches closer to your eardrum..." << endl;

				system("pause");
				system("cls");

				mmLeft -= mmBet;
				playerSlave.erase(playerSlave.begin() + chooseCard);

				if (mmLeft > 0)
				{
					cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
					bet(mmBet, mmLeft);
				}

				system("pause");
				system("cls");
			}
}

void playRound(int& round, int& mmLeft, int& moneyEarned)
{
	// Not sure if this should be here or playRound()
	/*srand(time(0));*/

	int placeBet;
	int threeBoutsEmp = 1;
	int threeBoutsSl = 1;

	vector <string> slaveCardsPlayer;
	vector <string> emperorCardsPlayer;
	vector <string> slaveCardsAI;
	vector <string> emperorCardsAI;

	cardsPlayerSlave(slaveCardsPlayer);
	cardsAiSlave(slaveCardsAI);
	cardsPlayerEmperor(emperorCardsPlayer);
	cardsAiEmperor(emperorCardsAI);

			while (threeBoutsEmp <= 3 && mmLeft > 0)
			{
				if (round == 3 || round == 9)
				{
					cout << "Alright, heads up. We're switching your cards to the Slave set after this round: says Tonegawa" << endl;
					system("pause");
					system("cls");
				}

				cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
				cout << "Money Earned:" << moneyEarned << " $" << endl;
				cout << "Round No.: " << round << endl;
				cout << "=================" << endl;

				emperorPlay(emperorCardsPlayer, slaveCardsAI, mmLeft, placeBet, moneyEarned);

				threeBoutsEmp++;
				round++;

			}

			// Must come BEFORE the bet 
			// I'm putting it here because I don't know where to put it
			// could have put it inside the while loop and an if function
			///*cout << "Alright, it's time to switch cards: says Tonegawa" << endl;
			//cout << "His assistants hand you the Slave Set" << endl;
			//system("pause");
			//system("cls");*/

			while (threeBoutsSl <= 3 && mmLeft > 0)
			{
				if (round == 6)
				{
					cout << "Alright, heads up. We're switching your cards to the Emperor set after this round:" << endl;
					system("pause");
					system("cls");
				}

				cout << "Milimiters left: " << mmLeft << " / 30mm" << endl;
				cout << "Money Earned:" << moneyEarned << " $" << endl;
				cout << "Round No.: " << round << endl;
				cout << "=================" << endl;

				slavePlay(slaveCardsPlayer, emperorCardsAI, mmLeft, placeBet, moneyEarned);

				threeBoutsSl++;
				round++;
			}

			// Must come BEFORE the bet 
		///*	cout << "Alright, it's time to switch cards: says Tonegawa" << endl;
		//	cout << "His assistants hand you the Emperor Set" << endl;
		//	system("pause");
		//	system("cls");*/
}

void willHeWin(int& mmLeft, int& moneyEarned)
{
	// Boring ending
	if (mmLeft > 0)
	{
		cout << "Tonegawa lets out a laugh. See? I was right: he says" << endl;
		cout << "It's not possible to win against me. You were wise to play it safe: Tonegawa smirks." << endl;
		cout << "Keep listening to my advice and I assure you that you will survive this ordeal of yours." << endl;
		cout << "As Tonegawa leaves you in the hall, a feeling of dread slowly creeps up to you" << endl;
		cout << "Perhaps, you are fated to stay here and pay off your debts for the rest of your life..." << endl;
		cout << endl;
		cout << "KAIJI IN LIMBO - ENDING 1" << endl;
	}

		// Bad ending
		if (mmLeft <= 0)
		{
			cout << "BRRRRRRRRRRRRR! The drill bores into your eardrum..." << endl;
			cout << "All you feel is a sharp pain at the side of your head before you pass out..." << endl;
			cout << endl;
			cout << "KAIJI LOSES HIS HEARING - ENDING 2" << endl;
		}

			// Good ending 
			if (moneyEarned >= 20000000)
			{
				// This doesn't happen in the series, since we all know Tonegawa cheated, but I'm doing this so the "script" is shorter
				cout << "Tonegawa stares at you in disbelief." << endl;
				cout << "How?: He asks you." << endl;
				cout << "You aren't so sure yourself, so you just shrug and smile." << endl;
				cout << "You stand up and walk out the door." << endl;
				cout << "Finally you are free." << endl;
				cout << endl;
				cout << "KAIJI PAYS OFF HIS DEBTS - ENDING 3" << endl;
			}
}

// Put WIN conditions inside main, after the while loop 

int main()
{
	srand(time(0));
	int playBet;
	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;

	begin();
	system("cls");
	bet(playBet, mmLeft);
	system("cls");

	while (round <= 12 && mmLeft > 0)
	{
		playRound(round, mmLeft, moneyEarned);
	}

	willHeWin(mmLeft, moneyEarned);
}