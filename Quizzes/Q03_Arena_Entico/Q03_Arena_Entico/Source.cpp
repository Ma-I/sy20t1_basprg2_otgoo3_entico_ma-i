#include <iostream>
#include<string>
#include <time.h>
#include "Unit.h"

using namespace std;

void giveName(string& playerName)
{
	cout << "Welcome adventurer!" << endl;
	cout << "What is your name?: ";
	cin >> playerName;
}

void chooseClass(string& playerName, char& classPick, Unit * player)
{
	cout << "Pick your class: " << endl;
	cout << "[a] Warrior" << endl;
	cout << "[s] Mage" << endl;
	cout << "[d] Assassin" << endl;

	cin >> classPick;

	if (classPick != 'a' && classPick != 's' && classPick != 'd')
	{
		do
		{

			cout << "Please choose an existing class...";
			cin >> classPick;

		} while (classPick != 'a' && classPick != 's' && classPick != 'd');
	}

	switch (classPick)
	{
	// Warrior
	// Strong against Assassin 
	// Weak agaisnt Mage
	// Plus 3 POW and VIT
	case 'a': 
		player->setClass("Warrior");
		player->setHPmax(17);
		player->setHPmin(17);
		player->setPOW(7);
		player->setAGI(3);
		player->setDEX(5);
		player->setVIT(4);
		break;

	// Mage
	// Strong against Warrior 
	// Weak agaisnt Assassin
	// Plus 5 POW
	case 's':
		player->setClass("Mage");
		player->setHPmax(15);
		player->setHPmin(15);
		player->setPOW(7);
		player->setAGI(3);
		player->setDEX(4);
		player->setVIT(5);
		break;

	// Assassin
	// Strong against Mage 
	// Weak agaisnt Warrior
	// Plus 3 AGI and DEX
	case 'd':
		player->setClass("Assassin");
		player->setHPmax(16);
		player->setHPmin(16);
		player->setPOW(8);
		player->setAGI(4);
		player->setDEX(6);
		player->setVIT(3);
		break;
	}
}

void enemyAI(Unit * enemy, int pickClassAI)
{
	pickClassAI; 

	if (pickClassAI == 1)
	{
		enemy->setClass("Assassin");
		enemy->setHPmax(16);
		enemy->setHPmin(16);
		enemy->setPOW(8);
		enemy->setAGI(4);
		enemy->setDEX(6);
		enemy->setVIT(3);
	}

	if (pickClassAI == 2)
	{
		enemy->setClass("Mage");
		enemy->setHPmax(15);
		enemy->setHPmin(15);
		enemy->setPOW(7);
		enemy->setAGI(3);
		enemy->setDEX(4);
		enemy->setVIT(5);
	}

	if (pickClassAI == 3)
	{
		enemy->setClass("Warrior");
		enemy->setHPmax(17);
		enemy->setHPmin(17);
		enemy->setPOW(7);
		enemy->setAGI(3);
		enemy->setDEX(5);
		enemy->setVIT(4);
	}
}

void display(Unit * player, Unit * enemy, string playerName)
{
	cout << "PLAYER: " << playerName << endl;
	cout << "CLASS: " << player->getClass() << endl;
	cout << "HP: " << player->getHPmin() << " / " << player->getHPmax() <<endl;
	cout << "POW: " << player->getPOW() << endl;
	cout << "DEX: " << player->getDEX() << endl;
	cout << "AGI: " << player->getAGI() << endl;
	cout << "VIT: " << player->getVIT() << endl;

	cout << " ++++++++++++ " << endl;

	cout << "ENEMY" << endl;
	cout << "CLASS: " << enemy->getClass() << endl;
	cout << "HP: " << enemy->getHPmin() << " / " << player->getHPmax() << endl;
	cout << "POW: " << enemy->getPOW() << endl;
	cout << "DEX: " << enemy->getDEX() << endl;
	cout << "AGI: " << enemy->getAGI() << endl;
	cout << "VIT: " << enemy->getVIT() << endl;

	cout << "-----------------------" << endl; 
	cout << endl;
}

void gameOver(Unit * player, int rounds)
{
	cout << "Darkness overwhelms you...." << endl; 
	cout << "Hopefully someone will come along and resuce your poor soul..." << endl; 
	cout << endl; 
	cout << "You got as far as " << rounds << " rounds" << endl;
	cout << endl; 
	cout << "You were able to attain: " << endl;
	cout << player->getPOW() << " power points" << endl;
	cout << player->getDEX() << " dexterity points" << endl;
	cout << player->getAGI() << " agility points" << endl;
	cout << player->getVIT() << " vitality points" << endl;
	cout << endl; 
	cout << "May the RNG gods smile down upon you in your next life..." << endl; 
	cout << endl;
	cout << "=== GAME OVER ===" << endl;

}

int main()
{
	srand(time(NULL));
	string playerName; 
	char classPick;
	int roundCounter = 1;

	giveName(playerName);

	Unit* player = new Unit("default");
	// default placeholders
	player->setHPmax(1);
	player->setHPmin(1);
	player->setPOW(1);
	player->setAGI(1);
	player->setDEX(1);
	player->setVIT(1);

	chooseClass(playerName, classPick, player);

	system("cls");

	while (player->getHPmin() > 0)
	{
		int pickClassAI = rand() % 3 + 1;

		Unit* enemy = new Unit("default");
		// default placeholders
		enemy->setHPmax(1);
		enemy->setHPmin(1);
		enemy->setPOW(1);
		enemy->setAGI(1);
		enemy->setDEX(1);
		enemy->setVIT(1);

		enemyAI(enemy, pickClassAI);

		if (roundCounter > 1)
		{
			enemy->scaleDifficulty();
		}

		while (enemy->getHPmin() > 0 && player->getHPmin() > 0)
		{
			cout << "ROUND: " << roundCounter << endl;
			display(player, enemy, playerName);

			if (player->getAGI() > enemy->getAGI())
			{
				cout << "You attack ..." << endl; 

				player->attackUnit(enemy);
				cout << endl;

				cout << "Your enemy attacks ..." << endl;

				enemy->attackUnit(player);
				cout << endl;
			}
			if (enemy->getAGI() > player->getAGI())
			{
				cout << "Your enemy attacks ..." << endl;

				enemy->attackUnit(player);
				cout << endl;

				cout << "You attack ..." << endl;

				player->attackUnit(enemy);
				cout << endl;
			}
			if (player->getAGI() == enemy->getAGI())
			{
				cout << "You attack ..." << endl;

				player->attackUnit(enemy);
				cout << endl;

				cout << "Your enemy attacks ..." << endl;

				enemy->attackUnit(player);
				cout << endl;
			}

			system("pause");
			system("cls");
		}

		if (player->getHPmin() > 0)
		{
			player->levelUp(enemy);
			roundCounter++;
			system("pause");
			system("cls");
		}

		delete enemy; 
		enemy = nullptr;
	}

	gameOver(player, roundCounter);
	system("pause");
}