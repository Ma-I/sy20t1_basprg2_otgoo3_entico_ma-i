#include <iostream>
#include "Unit.h"


Unit::Unit(string name)
{
    // default placeholders
    mName = name;
    mHPmax = 1;
    mHPmin = 1;
    mPOW = 1;
    mVIT = 1;
    mAGI = 1;
    mDEX = 1;
}

void Unit::attackUnit(Unit* target)
{
    int hit = ((float)this->getDEX() / (float)target->getAGI()) * 100;
    int rate = rand() % 100 - 1;

    if (hit > 80)
    {
        hit = 80;
    }
    if (hit < 20)
    {
        hit = 20;
    }

    int bonus = (float)this->getPOW() * 0.5;
    int attackBonusDamage = ((float)this->getPOW() - (float)target->getVIT()) + bonus;
    int regularDamage = ((float)this->getPOW() - (float)target->getVIT());

    if (attackBonusDamage < 1)
    {
        attackBonusDamage = 1;
    }
    if (regularDamage < 1)
    {
        regularDamage = 1;
    }

    int attack = rand() % regularDamage + 1;
    int attackCritical = rand() % attackBonusDamage + 1;

    cout << this->getClass() << " attacks " << target->getClass() << endl; 

    if (rate < hit)
    {
        // Strong against
        if (getClass() == "Warrior" && target->getClass() == "Assassin" ||
            getClass() == "Mage" && target->getClass() == "Warrior" ||
            getClass() == "Assassin" && target->getClass() == "Mage")
        {
            target->setHPmin(target->getHPmin() - attackCritical);
            cout << this->getClass() << " dealt " << attackCritical << " damage!" << endl;
        }

        else
        {
            target->setHPmin(target->getHPmin() - attack);
            cout << this->getClass() << " dealt " << attack << " damage!" << endl;
        }
    }
    else
    {
        cout << this->getClass() << "'s attack missed!" << endl; 
    }
}

void Unit::levelUp(Unit * enemy)
{
    // heal 30% of health 
    int heal = (float)this->getHPmax() * 0.3;
    this->setHPmin(this->getHPmin() + heal);

    if (heal <= 0)
    {
        heal = 1;
    }
    if (heal > this->getHPmax())
    {
        heal = this->getHPmax();
    }

    cout << "Your HP heals by " << heal << " points..." << endl;

    // level up 
    cout << "You have levelled up..." << endl;

    if (enemy->getClass() == "Warrior")
    {
        cout << "POW + 3..." << endl;
        cout << "VIT + 3..." << endl;
        this->setPOW(getPOW() + 3);
        this->setVIT(getVIT() + 3);
    }
    if (enemy->getClass() == "Mage")
    {
        cout << "POW + 5..." << endl;
        this->setPOW(getPOW() + 5);
    }
    if (enemy->getClass() == "Assassin")
    {
        cout << "DEX + 3..." << endl;
        cout << "AGI + 3..." << endl;
        this->setDEX(getDEX() + 3);
        this->setAGI(getAGI() + 3);
    }

}

void Unit::scaleDifficulty()
{
    int pickStat = rand() % 4 + 1;
    int newHP = rand() % 25 + 10;

    if (pickStat == 1)
    {
        this->setPOW(getPOW() + 1);
        this->setVIT(getVIT() + 1);
    }

    if (pickStat == 2)
    {
        this->setPOW(getPOW() + 2);
    }

    if (pickStat == 3)
    {
        this->setDEX(getDEX() + 1);
        this->setAGI(getAGI() + 1);
    }
    if (pickStat == 4)
    {
        this->setHPmax(newHP);
    }
}

string Unit::getClass()
{
    return string(mName);
}

int Unit::getHPmax()
{
    return mHPmax;
}

int Unit::getHPmin()
{
    return mHPmin;
}

int Unit::getPOW()
{
    return mPOW;
}

int Unit::getVIT()
{
    return mVIT;
}

int Unit::getAGI()
{
    return mAGI;
}

int Unit::getDEX()
{
    return mDEX;
}

void Unit::setClass(string className)
{
    mName = className; 
}

void Unit::setHPmax(int hp)
{
    mHPmax = hp;

    if (hp < 0)
    {
        hp = 0;
    }
}

void Unit::setHPmin(int hp)
{
    mHPmin = hp;

    if (hp < 0)
    {
        hp = 0;
    }
}

void Unit::setPOW(int pow)
{
    mPOW = pow;
}

void Unit::setVIT(int vit)
{
    mVIT = vit;
}

void Unit::setAGI(int agi)
{
    mAGI = agi;
}

void Unit::setDEX(int dex)
{
    mDEX = dex;
}
