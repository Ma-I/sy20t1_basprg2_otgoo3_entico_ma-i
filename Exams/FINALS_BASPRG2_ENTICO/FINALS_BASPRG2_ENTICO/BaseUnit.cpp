#include "BaseUnit.h"

BaseUnit::BaseUnit(string className, int HPmax, int HPcurr, int pow, int vit, int agi, int dex)
{
    // default placeholders
    mClass = className;
    mHPmax = HPmax;
    mHPcurr = HPcurr;
    mPOW = pow;
    mVIT = vit;
    mAGI = agi;
    mDEX = dex;
}

BaseUnit::~BaseUnit()
{
}

void BaseUnit::attackUnit(BaseUnit* target)
{
    int hit = ((float)this->getDEX() / (float)target->getAGI()) * 100;
    int rate = rand() % 100 + 1;

    if (hit > 80)
    {
        hit = 80;
    }
    if (hit < 20)
    {
        hit = 20;
    }

    int attack = (this->getPOW()) - (target->getVIT());

    if (attack < 0)
    {
        attack = 1;
    }

    if (rate < hit)
    {
        target->setHPcurr(target->getHPcurr() - attack);
        cout << attack << " damage was dealt" << endl;
    }
    else
    {
        cout << "The attack missed!" << endl;
    }
}

string BaseUnit::getClass()
{
    return mClass;
}

int BaseUnit::getHPmax()
{
    return mHPmax;
}

int BaseUnit::getHPcurr()
{
    return mHPcurr;
}

int BaseUnit::getPOW()
{
    return mPOW;
}

int BaseUnit::getVIT()
{
    return mVIT;
}

int BaseUnit::getAGI()
{
    return mAGI;
}

int BaseUnit::getDEX()
{
    return mDEX;
}

void BaseUnit::setClass(string className)
{
    mClass = className;
}

void BaseUnit::setHPmax(int hp)
{
    mHPmax = hp;

    if (hp < 0)
    {
        hp = 0;
    }
}

void BaseUnit::setHPcurr(int hp)
{
    mHPcurr = hp;

    if (hp < 0)
    {
        hp = 0;
    }
}

void BaseUnit::setPOW(int pow)
{
    mPOW = pow;
}

void BaseUnit::setVIT(int vit)
{
    mVIT = vit;
}

void BaseUnit::setAGI(int agi)
{
    mAGI = agi;
}

void BaseUnit::setDEX(int dex)
{
    mDEX = dex;
}
