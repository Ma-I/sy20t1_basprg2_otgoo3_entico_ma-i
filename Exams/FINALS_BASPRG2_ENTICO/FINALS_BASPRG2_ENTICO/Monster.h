#pragma once
#include "BaseUnit.h"
#include "Player.h"

class Monster : public BaseUnit
{
public: 

	Monster();
	~Monster() {};

	void giveRewards(Player* hero);

	// Getters
	int getGold(); 
	int getEXPhero();

	// Setters  
	void setGold(int gold);
	void setEXPhero(int exp);

	// provided gold
	// provided exp 
	// spawn chance
private: 

	int mGoldHero; 
	int mEXPhero;
};

