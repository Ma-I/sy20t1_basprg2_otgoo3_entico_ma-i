#pragma once
#include "BaseUnit.h"
#include "Weapon.h"
#include "Armour.h"

class Player : public  BaseUnit
{
public: 

	Player();
	~Player() {}; 

	void movePlayer(char move);
	void levelUP();
	void playerRest();

	// make variable for a weapon
	void equipWeapon(Weapon* weapon);
	void unequipWeapon(Weapon* activeWeapon);
	void equipArmour(Armour* armour);
	void unequipArmour(Armour* activeArmour);

	// Getters
	string getName();
	string getWeapon();
	string getArmour();
	int getXpos();
	int getYpos();
	int getEXPcurr();
	int getEXPmax();
	int getLevel();
	int getPlayerGold();

	// Setters
	void setName(string name);
	void setWeapon(string name);
	void setArmour(string name);
	void setXpos(int xPos);
	void setYpos(int yPos);
	void setEXPcurr(int expCurr);
	void setEXPmax(int expMax);
	void setLevel(int lvl);
	void setPlayerGold(int gold);

private: 

	string mName;
	string mWeapon;
	string mArmour;

	int mxPos; 
	int myPos; 

	int mEXPcurr;
	int mEXPmax;
	int mLevel; 
	int mGold;
};

