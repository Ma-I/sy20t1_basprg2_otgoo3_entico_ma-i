#include "BaseUnit.h"
#include "Player.h"
#include "Monster.h"
#include "Armour.h"
#include "Weapon.h"
#include "Shop.h"
#include <time.h>

void playerClass(Player*hero)
{
	char choice;
	string name;

	cout << "What is your name?: "; 
	cin >> name; 

	hero->setName(name);

	do { 

		cout << "Choose your class: " << endl;
		cout << "[1] Warrior" << endl;
		cout << "[2] Theif" << endl;
		cout << "[3] Crusader" << endl;

		cout << "Choice: ";
		cin >> choice;

		if (choice <= '0' || choice >= '4')
		{
			cout << endl;
			cout << "Pick a valid class" << endl; 
			system("pause");
			system("cls");
		}

	} while (choice <= '0' || choice >= '4');

	if (choice == '1')
	{
		hero->setClass("Warrior");
		hero->setLevel(1);
		hero->setHPmax(25); 
		hero->setHPcurr(25); 
		hero->setPOW(15);
		hero->setVIT(15); 
		hero->setAGI(13);
		hero->setDEX(13);
		hero->setEXPmax(1000); 
		hero->setEXPcurr(0);
	}
	if (choice == '2')
	{
		hero->setClass("Theif");
		hero->setLevel(1);
		hero->setHPmax(20);
		hero->setHPcurr(20);
		hero->setPOW(13);
		hero->setVIT(16);
		hero->setAGI(15);
		hero->setDEX(15);
		hero->setEXPmax(1000);
		hero->setEXPcurr(0);
	}
	if (choice == '3')
	{
		hero->setClass("Crusader");
		hero->setLevel(1);
		hero->setHPmax(27);
		hero->setHPcurr(27);
		hero->setPOW(15);
		hero->setVIT(17);
		hero->setAGI(12);
		hero->setDEX(12);
		hero->setEXPmax(1000);
		hero->setEXPcurr(0);
	}
}

void userInterface(Player*hero)
{
	cout << "HERO: " << hero->getName() << " :: ";
	cout << hero->getClass() << endl;
	cout << "WEAPON: " << hero->getWeapon() << endl;
	cout << "ARMOUR: " << hero->getArmour() << endl;
	cout << "LVL: " << hero->getLevel() << endl;
	cout << "GOLD: " << hero->getPlayerGold() << endl;
	cout << "HP: " << hero->getHPcurr() << " / " << hero->getHPmax() << endl;
	cout << "POW: " << hero->getPOW() << endl;
	cout << "VIT: " << hero->getVIT() << endl;
	cout << "AGI: " << hero->getAGI() << endl;
	cout << "DEX: " << hero->getDEX() << endl;
	cout << "EXP: " << hero->getEXPcurr() << " / " << hero->getEXPmax() << endl;
}

void mainMenu(Player* hero, char& choice)
{
	do {
		cout << "MAP: " << "( " << hero->getXpos() << " , " << hero->getYpos() << " )" << endl;
		cout << "Main Menu: " << endl;
		cout << "[1] Move" << endl;
		cout << "[2] Rest" << endl;
		cout << "[3] Stats" << endl;
		cout << "[4] Quit" << endl;

		cout << "Choice: ";
		cin >> choice;

	} while (choice <= 0 && choice >= 5);
}

void spawnEnemy(Monster* enemy, bool& battle)
{
	int spawnRoll = rand() % 100 + 1;

	if (spawnRoll >= 1 && spawnRoll <= 25)
	{
		enemy->setClass("Goblin");
		enemy->setHPmax(10);
		enemy->setHPcurr(10);
		enemy->setPOW(10);
		enemy->setVIT(10);
		enemy->setAGI(8);
		enemy->setDEX(8);
		enemy->setGold(10);
		enemy->setEXPhero(100);

		cout << enemy->getClass() << " appeared!" << endl;
		cout << endl;
	}
	if (spawnRoll >= 26 && spawnRoll <= 50)
	{
		enemy->setClass("Ogre");
		enemy->setHPmax(20);
		enemy->setHPcurr(20);
		enemy->setPOW(15);
		enemy->setVIT(10);
		enemy->setAGI(10);
		enemy->setDEX(11);
		enemy->setGold(50);
		enemy->setEXPhero(250);

		cout << enemy->getClass() << " appeared!" << endl;
		cout << endl;
	}
	if (spawnRoll >= 51 && spawnRoll <= 75)
	{
		enemy->setClass("Orc");
		enemy->setHPmax(30);
		enemy->setHPcurr(30);
		enemy->setPOW(20);
		enemy->setVIT(13);
		enemy->setAGI(14);
		enemy->setDEX(14);
		enemy->setGold(100);
		enemy->setEXPhero(500);

		cout << enemy->getClass() << " appeared!" << endl;
		cout << endl;
	}
	if (spawnRoll >= 76 && spawnRoll <= 95)
	{
		battle = false;
	}
	if (spawnRoll >= 96 && spawnRoll <= 100)
	{
		enemy->setClass("Orc Lord");
		enemy->setHPmax(1000);
		enemy->setHPcurr(1000);
		enemy->setPOW(100);
		enemy->setVIT(50);
		enemy->setAGI(50);
		enemy->setDEX(50);
		enemy->setGold(1000);
		enemy->setEXPhero(1000);

		cout << enemy->getClass() << " appeared!" << endl;
		cout << endl;
	}

}

void runBattle(Player* hero, Monster* enemy, bool& battle)
{
	int runChance = rand() % 4 + 1;

	if (runChance == 1)
	{
		cout << "You run..." << endl;
		battle = false;
	}
	else
	{
		cout << "The " << enemy->getClass() << " attacks!" << endl;
		enemy->attackUnit(hero);
		cout << endl;
	}

}

void regularBattle(Player* hero, Monster* enemy)
{
	if (hero->getAGI() > enemy->getAGI())
	{
		cout << "You attack" << endl;
		hero->attackUnit(enemy);
		cout << endl;

		cout << "The " << enemy->getClass() << " attacks..." << endl;
		enemy->attackUnit(hero);
		cout << endl;
	}
	if (enemy->getAGI() > hero->getAGI())
	{
		cout << "The " << enemy->getClass() << " attacks..." << endl;
		enemy->attackUnit(hero);
		cout << endl;

		cout << "You attack" << endl;
		hero->attackUnit(enemy);
		cout << endl;
	}
	if (hero->getAGI() == enemy->getAGI())
	{
		cout << "You attack" << endl;
		hero->attackUnit(enemy);
		cout << endl;

		cout << "The " << enemy->getClass() << " attacks..." << endl;
		enemy->attackUnit(hero);
		cout << endl;
	}
}

void battleUI(Player* hero, Monster* enemy)
{
	cout << "===========================" << endl;
	cout << "HERO: " << hero->getName() << " :: ";
	cout << hero->getClass() << endl;
	cout << "HP: " << hero->getHPcurr() << " / " << hero->getHPmax() << endl;

	cout << "===========================" << endl;

	cout << enemy->getClass() << endl;
	cout << "HP: " << enemy->getHPcurr() << " / " << enemy->getHPmax() << endl;

	cout << "===========================" << endl;
	cout << endl;
}

void runOption(Player* hero, Monster* enemy, int& choice)
{
	battleUI(hero, enemy);

	do {
		cout << "What will you do?: " << endl;
		cout << "[1] run" << endl;
		cout << "[2] attack" << endl;
		cout << endl;
		cout << "Choice: ";
		cin >> choice;

		if (0 >= choice >= 3)
		{
			cout << endl;
			cout << "Input invalid..." << endl;
			system("pause");
			system("cls");
		}

	} while (0 >= choice >= 3);

	system("cls");
}

void BATTLE(Player* hero, Monster* enemy, bool& battle, int& choice)
{
	runOption(hero, enemy, choice);

	if (choice == 1)
	{
		battleUI(hero, enemy);
		runBattle(hero, enemy, battle);
		system("pause");
		system("cls");
	}
	if (choice == 2)
	{
		battleUI(hero, enemy);
		regularBattle(hero, enemy);
		system("pause");
		system("cls");
	}
}

int main() 
{
	srand(time(NULL)); 

	bool gameRun = true;
	Player* hero = new Player;
	Shop* secretShop = new Shop("+ Secret Shop +");


	playerClass(hero);
	system("cls");

	while (gameRun == true)
	{
		bool battle = true;
		bool shop = true;
		Monster* enemy = new Monster;
		char choice;
		mainMenu(hero, choice);
		system("cls");

		switch (choice)
		{
		case '1':
			char move;
			cout << "W: North || A: East || S: South || D: East" << endl;
			cin >> move;

			hero->movePlayer(move);
			system("cls");

			if (hero->getXpos() == 1 && hero->getYpos() == 1)
			{
				cout << "(1, 1) Secret Shop" << endl; 

				while (shop == true)
				{
					// Haven't figured out how to unequip and delete previously equipped items
					// whatever u equip become a permanent buff lol
					secretShop->setUpShop(hero, shop);
				}
			}

			else {
				spawnEnemy(enemy, battle);

				// Battle start
				while (battle == true && hero->getHPcurr() > 0 && enemy->getHPcurr() > 0)
				{
					int run;

					BATTLE(hero, enemy, battle, run);

					if (hero->getHPcurr() <= 0)
					{
						cout << "Game Over..." << endl;
						gameRun = false;
					}
				}

				// Rewards and Level Up 
				if (hero->getHPcurr() > 0 && enemy->getHPcurr() <= 0)
				{
					cout << "Enemy deafeated!" << endl;
					enemy->giveRewards(hero);
					cout << endl;

					// Level Up 
					hero->levelUP();
					system("pause");
					system("cls");
				}

				delete enemy;
				enemy = nullptr;
			}
			break;

		case '2':
			hero->playerRest();
			system("pause");
			system("cls");
			break;

		case '3':
			userInterface(hero);
			system("pause");
			system("cls");
			break;

		case '4':
			gameRun = false;
			break;

		default:
			cout << "Invalid input, try again..." << endl;
			system("pause");
			system("cls");
			break;
		}
	}
	
	delete hero;
	hero = nullptr;

	delete secretShop;
	secretShop = nullptr;
}