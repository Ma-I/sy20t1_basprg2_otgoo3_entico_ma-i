#pragma once

#include "Player.h"
#include <iostream>

class Shop
{
public:

	Shop(string name);
	~Shop() {};

	void setUpShop(Player* hero, bool& shop);
	void buyWeapon(Player* hero, bool& shop);
	void buyArmour(Player* hero, bool& shop);

private: 

	string mName;
};

