#pragma once
#include<string>

using namespace std;

class Weapon
{
public: 

	Weapon();
	~Weapon() {};

	// Getters
	string getWeaponName(); 
	int getDMG(); 
	int getCost(); 

	// Setters
	void setWeaponName(string name);
	void setDMG(int dmg);
	void setCost(int cost);

private: 

	string mWeaponName; 
	
	int mDmg; 
	int mCost;

};

