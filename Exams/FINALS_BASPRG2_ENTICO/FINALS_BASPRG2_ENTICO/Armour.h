#pragma once
#include<string>

using namespace std;

class Armour
{
public:

	Armour();
	~Armour() {};

	// Getters
	string getArmourName();
	int getDEF();
	int getACost();

	// Setters
	void setArmourName(string name);
	void setDEF(int def);
	void setACost(int cost);

private:

	string mArmourName;

	int mDef;
	int mACost;
};

