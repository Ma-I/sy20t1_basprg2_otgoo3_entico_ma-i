#include "Armour.h"

Armour::Armour()
{
	mArmourName = "";
	mDef = 0;
	mACost = 0;
}

string Armour::getArmourName()
{
	return mArmourName;
}

int Armour::getDEF()
{
	return mDef;
}

int Armour::getACost()
{
	return mACost;
}

void Armour::setArmourName(string name)
{
	mArmourName = name;
}

void Armour::setDEF(int def)
{
	mDef = def;
}

void Armour::setACost(int cost)
{
	mACost = cost;
}
