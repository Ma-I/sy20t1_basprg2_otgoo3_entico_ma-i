#include "Player.h"

Player::Player()
	: BaseUnit ("default class", 100, 100, 20, 20, 20, 20)
{
	mName = "default";
	mWeapon = "none";
	mArmour = "none";
	mxPos = 0; 
	myPos = 0; 
	mEXPmax = 0; 
	mEXPcurr = 0; 
	mLevel = 0;
	mGold = 0;
}

void Player::movePlayer(char move)
{
	switch (move)
	{
	case 'w':
		this->myPos = myPos + 1;
		break;
	case 'a':
		this->mxPos = mxPos - 1;
		break;
	case 's':
		this->myPos = myPos - 1;
		break;
	case 'd':
		this->mxPos = mxPos + 1;
		break;
	}
}

void Player::levelUP()
{
	if (this->getEXPcurr() >= this->getEXPmax())
	{
		setEXPcurr(getEXPcurr()-getEXPmax());

		setLevel(getLevel() + 1);

		setEXPmax(getLevel() * 1000);

		// Stats
		this->setHPmax(getHPmax() + rand() % 5);
		this->setPOW(getPOW() + rand() % 5);
		this->setVIT(getVIT() + rand() % 5);
		this->setAGI(getAGI() + rand() % 5);
		this->setDEX(getDEX() + rand() % 5);

		cout << "=========================" << endl;
		cout << "LEVEL UP!" << endl;
		cout << "~Your stats have improved~" << endl;
	}
	else
	{
		cout << "Gain more EXP to level up!" << endl; 
	}
}

void Player::playerRest()
{
	setHPcurr(getHPmax());
	
	cout << "Your HP has been restored!" << endl; 
}

void Player::equipWeapon(Weapon* weapon)
{
	this->setPOW(this->getPOW() + weapon->getDMG());
	this->setWeapon(weapon->getWeaponName());
}

void Player::unequipWeapon(Weapon* activeWeapon)
{
	this->setPOW(this->getPOW() - activeWeapon->getDMG());
}

void Player::equipArmour(Armour* armour)
{
	this->setVIT(this->getVIT() + armour->getDEF());
	this->setArmour(armour->getArmourName());
}

void Player::unequipArmour(Armour* activeArmour)
{
	this->setVIT(this->getVIT() - activeArmour->getDEF());
}

string Player::getName()
{
	return mName;
}

string Player::getWeapon()
{
	return mWeapon;
}

string Player::getArmour()
{
	return mArmour;
}

int Player::getXpos()
{
	return mxPos;
}

int Player::getYpos()
{
	return myPos;
}

int Player::getEXPcurr()
{
	return mEXPcurr;
}

int Player::getEXPmax()
{
	return mEXPmax;
}

int Player::getLevel()
{
	return mLevel;
}

int Player::getPlayerGold()
{
	return mGold;
}

void Player::setName(string name)
{
	mName = name;
}

void Player::setWeapon(string name)
{
	mWeapon = name;
}

void Player::setArmour(string name)
{
	mArmour = name;
}

void Player::setXpos(int xPos)
{
	mxPos = xPos;
}

void Player::setYpos(int yPos)
{
	myPos = yPos;
}

void Player::setEXPcurr(int expCurr)
{
	mEXPcurr = expCurr;
}

void Player::setEXPmax(int expMax)
{
	mEXPmax = expMax;
}

void Player::setLevel(int lvl)
{
	mLevel = lvl;
}

void Player::setPlayerGold(int gold)
{
	mGold = gold;
}
