#include "Monster.h"

Monster::Monster()
	: BaseUnit ("default class", 100, 100, 20, 20, 20, 20)
{
	mGoldHero = 0; 
	mEXPhero = 0; 
}

void Monster::giveRewards(Player* hero)
{
	hero->setPlayerGold(hero->getPlayerGold() + this->getGold());
	hero->setEXPcurr(hero->getEXPcurr() + this->getEXPhero());

	cout << "You gained: " << endl; 
	cout << this->getGold() << " GOLD" << endl; 
	cout << this->getEXPhero() << " EXP" << endl;
}

void Monster::setGold(int gold)
{
	mGoldHero = gold;
}

void Monster::setEXPhero(int exp)
{
	mEXPhero = exp;
}

int Monster::getGold()
{
	return mGoldHero;
}

int Monster::getEXPhero()
{
	return mEXPhero;
}
