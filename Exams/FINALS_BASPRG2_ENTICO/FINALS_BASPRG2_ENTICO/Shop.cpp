#include "Shop.h"

Shop::Shop(string name)
{
	mName = name;
}

void Shop::setUpShop(Player* hero, bool& shop)
{
	char choice;
	do {
		cout << "Welcome to " << this->mName << endl;
		cout << "What would you like to buy?: " << endl;
		cout << "[1] weapons" << endl;
		cout << "[2] armour" << endl;
		cout << "[3] Leave Shop" << endl;
		cout << "Choice: ";
		cin >> choice;

		if (choice > 3 && choice < 1)
		{
			cout << "Invalid input, try again..." << endl;
		}

	} while (choice > 3 && choice < 1);

	system("cls");

	if (choice == '1')
	{
		cout << "[1] Short Sword || +5 DMG (10 Gold)" << endl;
		cout << "[2] Long Sword  || +10 DMG (50 Gold)" << endl;
		cout << "[3] Broad Sword || +20 DMG (200 Gold)" << endl;
		cout << "[4] Excalibur   || +999 DMG (999 Gold)" << endl;
		cout << "[5] Leave Shop" << endl;
		this->buyWeapon(hero, shop);

		system("pause");
		system("cls");
	}
	if (choice == '2')
	{
		cout << "[1] Leather Mail || +2 DEF (50 Gold)" << endl;
		cout << "[2] Chain Mail   || +4 DEF (100 Gold)" << endl;
		cout << "[3] Plate Armour || +8 DEF (300 Gold)" << endl;
		cout << "[4] Leave Shop" << endl;
		this->buyWeapon(hero, shop);

		system("pause");
		system("cls");
	}
	if (choice == '3')
	{
		shop = false;
	}
}

void Shop::buyWeapon(Player* hero, bool& shop)
{
	char weapon; 

	do {

		cout << "Choice: ";
		cin >> weapon;

		if (weapon < 1 && weapon > 5)
		{
			cout << "Invalid input, please try again..." << endl;
		}

	} while (weapon < 1 && weapon > 5);

	if (weapon == '1')
	{
		Weapon* weapon = new Weapon;
		weapon->setWeaponName("Short Sword"); 
		weapon->setDMG(5);
		weapon->setCost(10);

		if (hero->getPlayerGold() >= weapon->getCost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - weapon->getCost());
			hero->equipWeapon(weapon);
			cout << "You bought " << weapon->getWeaponName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete weapon;
			weapon = nullptr;
		}
	}
	if (weapon == '2')
	{
		Weapon* weapon = new Weapon;
		weapon->setWeaponName("Long Sword");
		weapon->setDMG(10);
		weapon->setCost(50);

		if (hero->getPlayerGold() >= weapon->getCost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - weapon->getCost());
			hero->equipWeapon(weapon);
			cout << "You bought " << weapon->getWeaponName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete weapon;
			weapon = nullptr;
		}
	}
	if (weapon == '3')
	{
		Weapon* weapon = new Weapon;
		weapon->setWeaponName("Broad Sword");
		weapon->setDMG(20);
		weapon->setCost(200);

		if (hero->getPlayerGold() >= weapon->getCost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - weapon->getCost());
			hero->equipWeapon(weapon);
			cout << "You bought " << weapon->getWeaponName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete weapon;
			weapon = nullptr;
		}
	}
	if (weapon == '4')
	{
		Weapon* weapon = new Weapon;
		weapon->setWeaponName("Short Sword");
		weapon->setDMG(999);
		weapon->setCost(999);

		if (hero->getPlayerGold() >= weapon->getCost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - weapon->getCost());
			hero->equipWeapon(weapon);
			cout << "You bought " << weapon->getWeaponName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete weapon;
			weapon = nullptr;
		}
	}
	if (weapon == '5')
	{
		shop = false;
	}
}

void Shop::buyArmour(Player* hero, bool& shop)
{
	char armour;

	do {

		cout << "Choice: ";
		cin >> armour;

		if (armour < 1 && armour > 4)
		{
			cout << "Invalid input, please try again..." << endl;
		}

	} while (armour < 1 && armour > 4);

	if (armour == '1')
	{
		Armour* armour = new Armour;
		armour->setArmourName("Leather Mail");
		armour->setDEF(2);
		armour->setACost(50);

		if (hero->getPlayerGold() >= armour->getACost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - armour->getACost());
			hero->equipArmour(armour);
			cout << "You bought " << armour->getArmourName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete armour;
			armour = nullptr;
		}
	}
	if (armour == '2')
	{
		Armour* armour = new Armour;
		armour->setArmourName("Leather Mail");
		armour->setDEF(4);
		armour->setACost(100);

		if (hero->getPlayerGold() >= armour->getACost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - armour->getACost());
			hero->equipArmour(armour);
			cout << "You bought " << armour->getArmourName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete armour;
			armour = nullptr;
		}
	}
	if (armour == '3')
	{
		Armour* armour = new Armour;
		armour->setArmourName("Leather Mail");
		armour->setDEF(8);
		armour->setACost(300);

		if (hero->getPlayerGold() >= armour->getACost())
		{
			hero->setPlayerGold(hero->getPlayerGold() - armour->getACost());
			hero->equipArmour(armour);
			cout << "You bought " << armour->getArmourName() << " !" << endl;
		}
		else
		{
			cout << "Not enough gold!" << endl;
			delete armour;
			armour = nullptr;
		}
	}
	if (armour == '4')
	{
		shop = false;
	}
}