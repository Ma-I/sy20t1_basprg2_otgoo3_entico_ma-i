#include "Weapon.h"

Weapon::Weapon()
{
	mWeaponName = "default";
	mDmg = 0;
	mCost = 0;
}

string Weapon::getWeaponName()
{
	return mWeaponName;
}

int Weapon::getDMG()
{
	return mDmg;
}

int Weapon::getCost()
{
	return mCost;
}

void Weapon::setWeaponName(string name)
{
	mWeaponName = name;
}

void Weapon::setDMG(int dmg)
{
	mDmg = dmg;
}

void Weapon::setCost(int cost)
{
	mCost = cost;
}
