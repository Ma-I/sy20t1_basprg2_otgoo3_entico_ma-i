#pragma once
#include <iostream>
#include <string>

using namespace  std;

class BaseUnit
{
public:

	BaseUnit(string className, int HPmax, int HPcurr, int pow, int vit, int agi, int dex);
	~BaseUnit();

	void attackUnit(BaseUnit* target);

	// Getters
	string getClass();
	int getHPmax();
	int getHPcurr();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();

	// Setters
	void setClass(string className);
	void setHPmax(int hp);
	void setHPcurr(int hp);
	void setPOW(int pow);
	void setVIT(int vit);
	void setAGI(int agi);
	void setDEX(int dex);

private:

	string mClass;
	// Health points. Once it drops to zero, unit dies.
	int mHPmax;
	int mHPcurr;

	// Power.Each point directly adds 1 point to base damage.
	int mPOW;

	// Vitality. Each point reduces 1 point from damage received. This is applied on the base damage 
	// (before computing the 50% bonus damage, if applicable)
	// damage = (POW of attacker - VIT of defender) * bonusDamage
	int mVIT;

	// Agility.Determines evasion rate.
	// Decreases the likelihood of getting hit.Each point decreases the attacker�s hit rate.
	int mAGI;

	// Dexterity. Determines hit rate or chance of getting your attacks to land successfully.
	int mDEX;
};


